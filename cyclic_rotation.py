from random import sample, randint


def my_cyclic(a, k):
    # This was the best in time :)
    # 35.pstat 1ms
    return list(a[-k:] + a[:-k])


def my_solution(a, k):
    new_list = list()

    # Insert is costly, because it generates a new list
    # 36.pstat 184ms
    len_a = len(a)
    for i in range(len_a):
        new_position = (i + k) % len_a
        new_list.insert(new_position, a[i])

    return new_list


def solution(A, K):
    # 37.pstat 30ms
    result = [None] * len(A)

    for i in range(len(A)):
        result[(i + K) % len(A)] = A[i]

    return result


if __name__ == '__main__':
    my_array = [5, 3, 4, 1, 2]
    rotate_at = 2
    # print(f"{my_cyclic(my_array, rotate_at)=}")
    # print(f"{my_solution(my_array, rotate_at)=}")
    # print(f"{solution(my_array, rotate_at)=}")

    my_sample = sample(range(1, 100001), 99999)
    len_sample = len(my_sample)
    # print(f"{my_cyclic(my_sample, randint(2, len_sample))=}")
    # print(f"{my_solution(my_sample, randint(2, len_sample))=}")
    print(f"{solution(my_sample, randint(2, len_sample))=}")
