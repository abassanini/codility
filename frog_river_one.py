import random
from random import sample, shuffle


def my_solution_1(river_size: int, leaves: list):
    bridge = dict()
    i = 0
    while i < len(leaves):
        bridge[leaves[i]] = 1

        if len(bridge) == river_size:
            return i

        i += 1

    return -1


def my_solution_2(river_size: int, leaves: list):
    # Slowest
    bridge = list()

    for i, element in enumerate(leaves):
        if not element in bridge:
            bridge.append(element)
        if len(bridge) == river_size:
            return i

    return -1


def my_solution_3(river_size: int, leaves: list):
    bridge = [False] * (river_size + 1)

    for i, e in enumerate(leaves):
        if not bridge[e]:
            bridge[e] = True
            river_size -= 1
            if river_size == 0:
                return i
    return -1


def solution(X, A):
    river_positions = [False] * (X + 1)
    for time in range(len(A)):
        pos = A[time]
        if not river_positions[pos]:
            river_positions[pos] = True
            X -= 1
            if X == 0: return time
    return -1


if __name__ == '__main__':
    # river_size = 1
    # leaves = [1, 1, 1]

    river_size = 5
    leaves = [1, 3, 1, 4, 2, 3, 5, 4]

    len_sample = 100000
    my_sample = [random.randrange(1, len_sample) for i in range(len_sample)]

    # print(f"{my_solution_2(river_size, leaves)=}")
    # print(f"{my_solution_1(river_size, leaves)=}")
    # print(f"{my_solution_3(river_size, leaves)=}")
    # print(f"{solution(river_size, leaves)=}")

    # print(f"{my_solution_2(len_sample, my_sample)=}")
    print(f"{my_solution_1(len_sample, my_sample)=}")
    print(f"{my_solution_3(len_sample, my_sample)=}")
    print(f"{solution(len_sample, my_sample)=}")
