from random import sample


def my_solution(my_list):
    sum_my_list = sum(my_list)
    sum_all_numbers = sum(range(1, len(my_list) + 2))
    return sum_all_numbers - sum_my_list


def solution_hint_1(my_list):
    for x in range(1, len(my_list) + 1):
        if x not in my_list:
            return x


def solution_hint_2(my_list):
    control_list = {n: False for n in range(1, len(my_list) + 2)}

    for x in my_list:
        control_list[x] = True

    position = list(control_list.values()).index(False) + 1
    return position


def solution(A):
    actual_sum = 0
    for number in A:
        actual_sum += number
    max_number = len(A) + 1
    expected_sum = (max_number * (max_number + 1) // 2)
    return expected_sum - actual_sum


if __name__ == '__main__':
    # print(f"my_solution: {my_solution([])}")
    # print(f"solution_hint_1: {solution_hint_1([])}")
    # print(f"solution_hint_2: {solution_hint_2([])}")
    print(f"solution: {solution([])}")

    # print(f"my_solution: {my_solution([2, 3, 1, 6, 4, 7])}")
    # print(f"solution_hint_1: {solution_hint_1([2, 3, 1, 6, 4, 7])}")
    # print(f"solution_hint_2: {solution_hint_2([2, 3, 1, 6, 4, 7])}")
    print(f"solution: {solution([2, 3, 1, 6, 4, 7])}")

    # print(f"my_solution: "
    #       f"{my_solution(sample(range(1, 500001), 499999))}")
    # print(f"solution_hint_1: "
    #       f"{solution_hint_1(sample(range(1, 50001), 49999))}")
    # print(f"solution_hint_2: "
    #       f"{solution_hint_2(sample(range(1, 500001), 499999))}")
    print(f"solution: "
          f"{solution(sample(range(1, 500001), 499999))}")
