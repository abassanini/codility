# string input
# hello world string


def solution(string01: str):
    # import re
    # stripped_str = re.sub("\s+", " ", string01)
    # print(stripped_str)

    # list_string = string01.strip().split()
    list_string = string01.split()

    # print(list_string)
    # print(list_string[::-1])

    # new_str = ""

    # for e in reversed(list_string):
    #     new_str += f"{e.strip()} "
    # return new_str.strip()

    # i = len(list_string) - 1
    # while i >= 0:
    #     new_str += f"{list_string[i]} "
    #     i -= 1
    # return new_str.strip()

    return " ".join(list_string[::-1])


if __name__ == '__main__':
    my_string = "  Hello     World   "
    print(f"{solution(my_string)=}")
