import random


def my_solution_1(n, a):
    counters = [0] * n
    for element in a:
        if element < n:
            counters[element - 1] += 1
        else:
            counters = [max(counters)] * n

    return counters


def solution(N, A):
    counters = [0] * N
    start_line = 0
    current_max = 0
    for i in A:
        x = i - 1
        if i > N:
            start_line = current_max
        elif counters[x] < start_line:
            counters[x] = start_line + 1
        else:
            counters[x] += 1
        if i <= N and counters[x] > current_max:
            current_max = counters[x]
    for i in range(0, len(counters)):
        if counters[i] < start_line:
            counters[i] = start_line
    return counters


if __name__ == '__main__':
    # size = 5
    # instructions = [3, 4, 4, 6, 1, 4, 4]
    # print(f"{my_solution_1(size, instructions)=}")

    len_sample = 100000
    my_sample = [random.randrange(1, len_sample + 1) for i in
                 range(len_sample)]
    print(f"{my_solution_1(len_sample, my_sample)=}")
    print(f"{solution(len_sample, my_sample)=}")
