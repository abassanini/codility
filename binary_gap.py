import re


def binary_gap(my_int):
    my_reversed_int = str(int(f"{my_int:b}"[::-1]))
    max_gap = 0
    zero_pattern = "(0+)"
    zero_matches = re.findall(zero_pattern, my_reversed_int)
    if zero_matches:
        max_gap = len(max(zero_matches))

    return max_gap


# Two following obtained from S.Overflow:
# https://stackoverflow.com/questions/48951591/python-find-longest-binary-gap-in-binary-representation-of-an-integer-number

def binary_gap_one_line(N):
    return len(max(format(N, 'b').strip('0').split('1')))


def count_gap(x):
    """
        Perform Find the longest sequence of zeros between ones "gap" in binary representation of an integer
        Parameters
        ----------
        x : int
            input integer value

        Returns
        ----------
        max_gap : int
            the maximum gap length
    """
    try:
        # Convert int to binary
        b = "{0:b}".format(x)
        # Iterate from right to lift
        # Start detecting gaps after fist "one"
        for i, j in enumerate(b[::-1]):
            if int(j) == 1:
                max_gap = max([len(i) for i in b[::-1][i:].split('1') if i])
                break
    except ValueError:
        # print("Oops! no gap found")
        max_gap = 0
    return max_gap


if __name__ == '__main__':
    my_number = 2147483647
    print(f"Number: {my_number}, bg: {binary_gap(my_number)}")

    my_number = 2100480047
    print(f"Number: {my_number}, bg: {binary_gap(my_number)}")

    for my_number in range(1, 100):
        print(f"Number: {my_number}, bg: {binary_gap(my_number)}")

    # for my_number in range(2147480047, 2147483647):
    #     bg = binary_gap(my_number)
    #     cg = count_gap(my_number)
    #     print(f"Number: {my_number}, bg: {bg}")
    #     print(f"Number: {my_number}, bg: {bg}, cg: {cg}")
    #     if bg != cg:
    #         print(f"Error in {my_number}")
