from random import sample


def my_equilibrium(my_list):
    min_difference = None
    for p in range(len(my_list) - 1):
        left_sum = sum(my_list[:p + 1])
        right_sum = sum(my_list[p + 1:])

        difference = abs(sum(my_list[:p + 1]) - sum(my_list[p + 1:]))

        if p == 0 or difference < min_difference:
            min_difference = difference

    return min_difference


def equilibrium_hint_1(my_list: list):
    left_side_weight = right_side_weight = 0

    for x in range(len(my_list)):
        if left_side_weight <= right_side_weight:
            left_side_weight += my_list.pop(0)
        else:
            right_side_weight += my_list.pop(-1)

    return abs(left_side_weight - right_side_weight)


def equilibrium_hint_2(my_list: list):
    new_difference = None
    left_side = my_list[0]
    right_side = sum(my_list) - left_side
    difference = abs(left_side - right_side)

    for i in range(1, len(my_list)):
        left_side += my_list[i]
        right_side -= my_list[i]
        difference = abs(left_side - right_side)

        if i == 1 or difference < new_difference:
            new_difference = difference

    return new_difference


def solution(A: list):
    sum_left = A[0]
    sum_right = sum(A) - A[0]
    diff = abs(sum_left - sum_right)
    for i in range(1, len(A) - 1):
        sum_left += A[i]
        sum_right -= A[i]
        current_diff = abs(sum_left - sum_right)
        if diff > current_diff:
            diff = current_diff
    return diff


if __name__ == '__main__':
    # print(f"{my_equilibrium([3, 1, 2, 4, 3])=}")
    # print(f"{my_equilibrium([])=}")
    # print(f"{my_equilibrium(sample(range(1, 50001), 49999))=}")

    # print(f"{equilibrium_hint_1([3, 1, 2, 4, 3])=}")
    # print(f"{equilibrium_hint_1(sample(range(1, 100001), 99999))=}")
    # print(f"{equilibrium_hint_1(list(range(1, 100001)))=}")
    # print(f"{equilibrium_hint_1([])=}")

    print(f"{equilibrium_hint_2([3, 1, 2, 4, 3])=}")
    # print(f"{equilibrium_hint_2(sample(range(1, 100001), 99999))=}")
    # print(f"{equilibrium_hint_2(list(range(1, 100001)))=}")
    # print(f"{solution(list(range(1, 100001)))=}")
    print(f"{solution([3, 1, 2, 4, 3])=}")
